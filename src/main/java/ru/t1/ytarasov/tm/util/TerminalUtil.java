package ru.t1.ytarasov.tm.util;

import ru.t1.ytarasov.tm.exception.AbstractException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() throws IntegerInvalidException {
        try {
            final String value = SCANNER.nextLine();
            return Integer.parseInt(value);
        }
        catch (final Exception e) {
            throw new IntegerInvalidException();
        }
    }

    final class IntegerInvalidException extends AbstractException {

        public IntegerInvalidException() {
            super("Value is not Integer");
        }

    }
}
