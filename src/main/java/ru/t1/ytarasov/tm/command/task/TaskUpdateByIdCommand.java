package ru.t1.ytarasov.tm.command.task;

import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.model.Task;
import ru.t1.ytarasov.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    public static final String NAME ="task-update-by-id";

    public static final String DESCRIPTION = "Update task by id.";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final String userId = getAuthService().getUserId();
        final Task task = getTaskService().updateById(userId, id, name, description);
        showTask(task);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
