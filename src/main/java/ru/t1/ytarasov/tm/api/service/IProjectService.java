package ru.t1.ytarasov.tm.api.service;

import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    Project create(String userId, String name) throws AbstractException;

    Project create(String userId, String name, String description) throws AbstractException;

    Project create(String userId, String name, Status status) throws AbstractException;

    Project create(String userId, String name, String description, Status status) throws AbstractException;

    Project updateById(String userId, String id, String name, String description) throws AbstractException;

    Project updateByIndex(String userId, Integer index, String name, String description) throws AbstractException;

    void changeProjectStatusById(String userId, String id, Status status) throws AbstractException;

    void changeProjectStatusByIndex(String userId, Integer index, Status status) throws AbstractException;

}